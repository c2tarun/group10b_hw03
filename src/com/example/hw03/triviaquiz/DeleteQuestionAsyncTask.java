package com.example.hw03.triviaquiz;

import android.os.AsyncTask;

public class DeleteQuestionAsyncTask extends AsyncTask<Void, Void, Void> {

	TriviaQuiz quiz;
	DeleteQuestion deleter;
	
	public DeleteQuestionAsyncTask(DeleteQuestion deleter) {
		// TODO Auto-generated constructor stub
		this.deleter = deleter;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		deleter.deleteStarted();
		quiz = new TriviaQuizImpl();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		quiz.deleteAllQuestions();
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		deleter.deleteCompleted();
	}
	
	public interface DeleteQuestion {
		public void deleteStarted();
		public void deleteCompleted();
	}

}
