package com.example.hw03;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hw03.triviaquiz.DeleteQuestionAsyncTask;
import com.example.hw03.triviaquiz.DeleteQuestionAsyncTask.DeleteQuestion;

public class MainActivity extends Activity implements DeleteQuestion {

	Button startTriviaButton;
	Button createQuizButton;
	Button deleteAllButton;
	Button exitButton;

	ProgressDialog deleteProgress;
	
	public static final int START_TRIVIA = 001;
	public static final int NEW_QUESTION = 002;
	public static final int RESULT_ACTIVITY = 003;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startTriviaButton = (Button) findViewById(R.id.startTriviaButton);
		createQuizButton = (Button) findViewById(R.id.createQuizButton);
		deleteAllButton = (Button) findViewById(R.id.deleteAllButton);
		exitButton = (Button) findViewById(R.id.exitButton);

		startTriviaButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, StartTrivia.class);
				startActivityForResult(intent, START_TRIVIA);
			}
		});

		createQuizButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, NewQuestion.class);
				startActivityForResult(intent, NEW_QUESTION);
			}
		});

		deleteAllButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DeleteAlertDialog deleteAlert = new DeleteAlertDialog(
						MainActivity.this);
				deleteAlert.show(getFragmentManager(), "Delete");
			}
		});
		
		exitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	class DeleteAlertDialog extends DialogFragment {

		private MainActivity activity;

		public DeleteAlertDialog(MainActivity activity) {
			// TODO Auto-generated constructor stub
			this.activity = activity;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(
					activity.getResources().getString(
							R.string.delete_ques_title))
					.setMessage(
							activity.getResources().getString(
									R.string.delete_ques_message))
					.setPositiveButton(R.string.positive_delete,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO: call delete from trivia game impl
									new DeleteQuestionAsyncTask(activity)
											.execute();
								}
							})
					.setNegativeButton(R.string.negative_delete,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO: DO nothing
								}
							});
			return builder.create();
		}
	}

	@Override
	public void deleteStarted() {
		// TODO Auto-generated method stub
		deleteProgress = new ProgressDialog(this);
		deleteProgress.setTitle(getResources().getString(R.string.delete_ques_progress_title));
		deleteProgress.setMessage(getResources().getString(R.string.delete_ques_progress_message));
		deleteProgress.show();
	}

	@Override
	public void deleteCompleted() {
		// TODO Auto-generated method stub
		deleteProgress.dismiss();
	}
}
