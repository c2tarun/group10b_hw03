package com.example.hw03;

import java.util.ArrayList;

import android.os.AsyncTask;

import com.example.hw03.triviaquiz.Question;
import com.example.hw03.triviaquiz.TriviaQuizImpl;

public class TestThread extends AsyncTask<Void, Void, Void> {

	@Override
	protected Void doInBackground(Void... params) {
		TriviaQuizImpl quiz = new TriviaQuizImpl();
		/*
		 * List<Question> questions = quiz.getAllQuestions(); for(Question ques
		 * : questions) { Log.d("demo", ques.toString()); }
		 */
		ArrayList<String> answers = new ArrayList<String>();
		answers.add("Beizing");
		answers.add("Hong Kong");
		answers.add("Tokyo");
		Question ques = new Question(
				"What is Capital of China?",
				answers,
				"",
				1);
		try {
			quiz.saveNewQuestion(ques);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
