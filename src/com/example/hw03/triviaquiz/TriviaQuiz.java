package com.example.hw03.triviaquiz;

import java.util.List;

public interface TriviaQuiz {

	public static final String GROUP_ID = "8jUZLCWEywLED7zA6Ph2";
	public static final String GET_ALL_URL = "http://dev.theappsdr.com/apis/trivia/getAll.php";
	public static final String SAVE_NEW_API = "http://dev.theappsdr.com/apis/trivia/saveNew.php";
	public static final String DELETE_ALL_URL = "http://dev.theappsdr.com/apis/trivia/deleteAll.php";
	
	public List<Question> getAllQuestions();
	public int saveNewQuestion(Question question);
	public void deleteAllQuestions();
	
}
