package com.example.hw03.triviaquiz;

import android.os.AsyncTask;

public class SaveQuestionAsyncTask extends AsyncTask<Question, Void, Integer>{
	
	SaveQuestion saver;
	TriviaQuiz quiz;
	
	public SaveQuestionAsyncTask(SaveQuestion saver) {
		this.saver = saver;
	}
	
	@Override
	protected void onPreExecute() {
		saver.saveStarted();
	}

	@Override
	protected Integer doInBackground(Question... params) {
		quiz = new TriviaQuizImpl();
		int responseCode = 0;
		try {
			responseCode = quiz.saveNewQuestion(params[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseCode;
	}
	
	@Override
	protected void onPostExecute(Integer responseCode) {
		saver.saveCompleted(responseCode);
	}
	public interface SaveQuestion {
		public void saveStarted();
		public void saveCompleted(int responseCode);
	}

}
