package com.example.hw03;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.hw03.triviaquiz.Question;
import com.example.hw03.triviaquiz.SaveQuestionAsyncTask;
import com.example.hw03.triviaquiz.SaveQuestionAsyncTask.SaveQuestion;
import com.example.hw03.util.Util;

public class NewQuestion extends Activity implements SaveQuestion{
	
	EditText questionText;
	EditText answerText;
	ImageView addAnswer;	// TODO: try to change this to ImageButton
	EditText imageUrlEditText;
	RadioGroup answerGroup;
	Button submitButton;
	
	ArrayList<String> answersList;
	ProgressDialog pd;
	
	private static int COUNTER = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_question);
		
		questionText = (EditText) findViewById(R.id.questionTextView);
		answerText = (EditText) findViewById(R.id.answerTextView);
		addAnswer = (ImageView) findViewById(R.id.addButton);
		imageUrlEditText = (EditText) findViewById(R.id.imageUrlTextView);
		answerGroup = (RadioGroup) findViewById(R.id.newAnswerGroup);
		submitButton = (Button) findViewById(R.id.submitButton);
		COUNTER = 0;
		
		answersList = new ArrayList<String>();
		
		addAnswer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String answer = answerText.getText().toString();
				if(!Util.isEmpty(answer)) {
					RadioButton rb = new RadioButton(NewQuestion.this);
					rb.setText(answer);
					rb.setId(COUNTER);
					if(COUNTER == 0) {
						rb.setChecked(true);
					}
					COUNTER++;
					answerGroup.addView(rb);
					answersList.add(answer);
					answerText.setText("");
				} else {
					Toast.makeText(NewQuestion.this, getResources().getString(R.string.missing_options), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		submitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean validQuestion = true;
				String quesStr = questionText.getText().toString();
				String imageUrl = imageUrlEditText.getText().toString();
				if(Util.isEmpty(quesStr)){
					validQuestion = false;
					Toast.makeText(NewQuestion.this, getResources().getString(R.string.missing_question), Toast.LENGTH_LONG).show();
				}
				if(Util.isEmpty(imageUrl)) {
					imageUrl = "";
				}
				if(COUNTER < 2) {
					validQuestion = false;
					Toast.makeText(NewQuestion.this, getResources().getString(R.string.less_options), Toast.LENGTH_LONG).show();
				}
				if(validQuestion) {
					int correctAnswer = answerGroup.getCheckedRadioButtonId();
					Question question = new Question(quesStr, answersList, imageUrl, correctAnswer);
					new SaveQuestionAsyncTask(NewQuestion.this).execute(question);
				}
				
			}
		});
	}

	@Override
	public void saveStarted() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(this);
		pd.setTitle(getResources().getString(R.string.save_ques_progress_title));
		pd.setMessage(getResources().getString(R.string.save_ques_progress_message));
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.show();
	}

	@Override
	public void saveCompleted(int responseCode) {
		// TODO Auto-generated method stub
		if(pd != null) {
			pd.dismiss();
		}
		if(responseCode != HttpURLConnection.HTTP_OK) {
			if(!Util.isConnectedOnline(this)) {
				Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, getResources().getString(R.string.unknown_reason), Toast.LENGTH_LONG).show();
			}
		} else {
			Intent intent = new Intent();
			setResult(MainActivity.NEW_QUESTION, intent);
			finish();
		}
	}
}
