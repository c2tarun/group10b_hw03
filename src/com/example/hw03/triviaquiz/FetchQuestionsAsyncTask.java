package com.example.hw03.triviaquiz;

import java.util.List;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class FetchQuestionsAsyncTask extends AsyncTask<String, Void, List<Question>> {
	TriviaQuiz quiz;
	QuestionsReceiver receiver;
	private static final String pdTitle = "Loading Questions";
	private static final String pdMessage = "Loading...";
	
	public FetchQuestionsAsyncTask(QuestionsReceiver receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		receiver.enableProgressDialog(pdTitle, pdMessage, ProgressDialog.STYLE_SPINNER);
	}

	@Override
	protected List<Question> doInBackground(String... params) {
		quiz = new TriviaQuizImpl();
		return quiz.getAllQuestions();
	}

	@Override
	protected void onPostExecute(List<Question> result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Log.d("demo", "Fetched " + result.size() + " valid questions.");
		receiver.disableProgressDialog();
		receiver.setQuestions(result);
	}
	
	public interface QuestionsReceiver {
		public void setQuestions(List<Question> questions);
		public void enableProgressDialog(String title, String message, int style);
		public void disableProgressDialog();
	}
}
