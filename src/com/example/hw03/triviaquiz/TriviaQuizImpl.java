package com.example.hw03.triviaquiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.hw03.util.Util;

import android.util.Log;

public class TriviaQuizImpl implements TriviaQuiz {

	List<Question> questions;

	@Override
	public List<Question> getAllQuestions() {
		InputStream in = null;
		HttpURLConnection con;
		try {
			URL url = new URL(GET_ALL_URL);
			con = (HttpURLConnection) url.openConnection();
			con.connect();
			in = con.getInputStream();
			return parseStream(in);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public int saveNewQuestion(Question question) {
		Log.d("demo", "Parsed Question " + parseQuestionToString(question));
		Map<String, String> params = new HashMap<String, String>();
		OutputStreamWriter writer = null;
		int responseCode = 0;
		try {
			params.put("q", parseQuestionToString(question));
			params.put("gid", GROUP_ID);
			URL url = new URL(SAVE_NEW_API);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			writer = new OutputStreamWriter(con.getOutputStream());
			writer.write(getEncodedParams(params));
			writer.flush();
			writer.close();
			responseCode = con.getResponseCode();
			Log.d("demo", "Question posted");
		} catch (UnsupportedEncodingException e) {
			Log.d("demo", "Question encoding failed");
		} catch (Exception e) {
			Log.d("demo", "Due to some reason question not posted. ");
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				Log.d("demo", "Writer closing failed");
			}
		}

		return responseCode;
	}

	private String getEncodedParams(Map<String, String> params)
			throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (String key : params.keySet()) {
			String value = URLEncoder.encode(params.get(key), "UTF-8");
			if (sb.length() > 0) {
				sb.append("&");
			}
			sb.append(key + "=" + value);
		}
		Log.d("demo", "Encoded Params " + sb.toString());
		return sb.toString();
	}

	public void deleteAllQuestions() {
		// TODO Auto-generated method stub
		URL url;
		OutputStreamWriter writer = null;
		try {
			url = new URL(DELETE_ALL_URL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			writer = new OutputStreamWriter(con.getOutputStream());
			writer.write("gid=" + GROUP_ID);
			writer.flush();
			con.getResponseCode();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private List<Question> parseStream(InputStream in) throws IOException {
		questions = new ArrayList<Question>();
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		while ((line = br.readLine()) != null) {
			Question ques = parseQuestion(line);
			if (ques != null) {
				questions.add(ques);
			}
		}
		return questions;
	}

	private Question parseQuestion(String line) {
		Question question = new Question();

		String[] splits = line.split(";");
		if (splits.length >= 5) { // 5 min elements, ques, ans1, ans2, url,
									// counter
			try {
				int length = splits.length;
				String quesStr = splits[0];
				if(!Util.isEmpty(quesStr))
					question.setQuestion(splits[0]);
				else
					return null;
				question.setImageUrl(splits[length - 2]);

				int correctAnswer = Integer.parseInt(splits[length - 1]);
				if (correctAnswer < 0 || correctAnswer >= (length - 3)) {
					Log.d("demo", "Index of Correct answer is wrong.");
					return null;
				}
				question.setCorrectAnswer(correctAnswer);
				ArrayList<String> answers = new ArrayList<String>();

				for (int i = 1; i < length - 2; i++) {
					String ansStr = splits[i];
					if(!Util.isEmpty(ansStr))
						answers.add(ansStr);
				}
				if(answers.size() >= 2)
					question.setAnswers(answers);
				else
					return null;

			} catch (NumberFormatException e) {
				Log.d("demo", "Index of Correct answer is wrong.");
				return null;
			}
			return question;
		} else {
			Log.d("demo", "Question is faulty");
			return null;
		}

	}

	private String parseQuestionToString(Question question) {
		StringBuilder sb = new StringBuilder();
		sb.append(question.getQuestion() + ";");
		ArrayList<String> answers = question.getAnswers();
		for (String ans : answers) {
			sb.append(ans + ";");
		}
		sb.append(question.getImageUrl() + ";");
		sb.append(question.getCorrectAnswer());

		return sb.toString();
	}

}
