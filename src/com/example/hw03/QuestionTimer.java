package com.example.hw03;

import android.os.CountDownTimer;
import android.util.Log;

public class QuestionTimer extends CountDownTimer {

	private static int COUNTER = 0;

	private static QuestionTimer singleton;
	private QuestionTimerReceiver receiver;
	
	public QuestionTimerReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(QuestionTimerReceiver receiver) {
		this.receiver = receiver;
	}

	private QuestionTimer(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);

	}
	
	public static QuestionTimer getInstance(QuestionTimerReceiver receiver) {
		if(COUNTER == 0) {
			singleton = new QuestionTimer(StartTrivia.TIME_PER_QUESTION, StartTrivia.ONE_SECOND);
			COUNTER++;
		}
		singleton.cancel();
		singleton.setReceiver(receiver);
		return singleton;
	}

	@Override
	public void onTick(long millisUntilFinished) {
		receiver.updateTimerText(millisUntilFinished);
	}

	@Override
	public void onFinish() {
		Log.d("demoStartActivity", "Timer finish called");
		receiver.updateScore();
		receiver.populateNextQuestion();
	}
	
	public interface QuestionTimerReceiver {
		public void updateTimerText(long millisUntilFinished);
		public void populateNextQuestion();
		public void updateScore();
	}

}
