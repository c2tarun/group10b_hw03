package com.example.hw03.triviaquiz;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class FetchImageAsyncTask extends AsyncTask<String, Void, Bitmap>{
	ImageReceiver receiver;
	String qId;
	
	private static final String TAG = FetchImageAsyncTask.class.getName();
	public FetchImageAsyncTask(ImageReceiver receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	
	@Override
	protected Bitmap doInBackground(String... params) {
		String imageUrl = params[0];
		qId = params[1];
		Log.d("demo", "Image URL " + imageUrl);
		Log.d("demo", "Question ID " + qId);
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.connect();
			Bitmap image = BitmapFactory.decodeStream(con.getInputStream());
			return image;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			Log.d(TAG, "Faulty URL");
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d(TAG, "I/O Exception while fetching image.");
		} catch (Exception e) {
			Log.d(TAG, "Image fetch failed, some problem with url.");
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Bitmap image) {
		// TODO Auto-generated method stub
		super.onPostExecute(image);
		receiver.setImage(image, qId);
	}

	
	public interface ImageReceiver {
		public void setImage(Bitmap image, String qId);
		public void enableImageLoadProgress(String title, String message, int style);
		public void disableImageLoadProgress();
	}
}
