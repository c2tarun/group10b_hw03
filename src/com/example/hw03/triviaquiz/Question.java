package com.example.hw03.triviaquiz;

import java.util.ArrayList;

public class Question {
	private String question;
	private ArrayList<String> answers; // Using ArrayList instead of List to
										// preserve order of options.
	private int correctAnswer;
	private String imageUrl;
	private int questionID;
	public static int QUESTION_ID_COUNTER = 0; 
	
	public Question() {
		this.questionID = ++QUESTION_ID_COUNTER;
	}

	public Question(String question, ArrayList<String> answers,
			String imageUrl, int correctAnswer) {
		super();
		this.question = question;
		this.answers = answers;
		this.correctAnswer = correctAnswer;
		this.imageUrl = imageUrl;
		this.questionID = ++QUESTION_ID_COUNTER;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public ArrayList<String> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<String> answers) {
		this.answers = answers;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Q. " + question + "\n" + "ANS:\n");
		for (String ans : answers) {
			sb.append(ans + ", ");
		}
		sb.append("\n" + "Image " + imageUrl + "\n");
		sb.append("Correct Ans: " + correctAnswer + "\n----------------\n");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + questionID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (questionID != other.questionID)
			return false;
		return true;
	}
	
}
