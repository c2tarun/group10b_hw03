package com.example.hw03.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Util {
	
	public static boolean isEmpty(String str) {
		if(str == null)
			return true;
		
		if(str.trim().length() == 0)
			return true;
		
		return false;
	}

	public static boolean isConnectedOnline(final Activity activity) {
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}
}
