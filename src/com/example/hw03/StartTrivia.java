package com.example.hw03;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.hw03.QuestionTimer.QuestionTimerReceiver;
import com.example.hw03.triviaquiz.FetchImageAsyncTask;
import com.example.hw03.triviaquiz.FetchImageAsyncTask.ImageReceiver;
import com.example.hw03.triviaquiz.FetchQuestionsAsyncTask;
import com.example.hw03.triviaquiz.FetchQuestionsAsyncTask.QuestionsReceiver;
import com.example.hw03.triviaquiz.Question;
import com.example.hw03.util.Util;

public class StartTrivia extends Activity implements QuestionsReceiver,
		ImageReceiver, QuestionTimerReceiver {

	private static int currentQIndex;
	private static int totalQuestions;
	private ProgressDialog pdQuestions;
	List<Question> questions;

	private int score;

	private TextView qIndex;
	private TextView qTimer;
	private ImageView qImage;
	private TextView qText;
	private RadioGroup answerGroup;
	private Button quitButton;
	private Button nextButton;
	private LinearLayout progressBarLayout;
	private QuestionTimer timer;

	protected static final long TIME_PER_QUESTION = 25000;
	protected static final long ONE_SECOND = 1000;
	public static final String PERCENT = "Percent";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz_layout);

		score = 0;

		qIndex = (TextView) findViewById(R.id.questionNumber);
		qTimer = (TextView) findViewById(R.id.timerView);
		progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayout);
		qImage = (ImageView) findViewById(R.id.questionImage);
		qText = (TextView) findViewById(R.id.questionView);
		answerGroup = (RadioGroup) findViewById(R.id.answerGroup);
		quitButton = (Button) findViewById(R.id.quitButton);
		nextButton = (Button) findViewById(R.id.nextButton);

		quitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				if (timer != null)
					timer.cancel();
				setResult(MainActivity.START_TRIVIA, intent);
				finish();
			}
		});

		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				updateScore();
				populateNextQuestion();
			}
		});
		new FetchQuestionsAsyncTask(this).execute();

	}

	public void updateScore() {
		Question question = questions.get(currentQIndex - 1);
		int checkedAnswer = answerGroup.getCheckedRadioButtonId();
		if (question.getCorrectAnswer() == checkedAnswer) {
			Log.d("demoStartTrivia", "Correct " + checkedAnswer + " " + question.getCorrectAnswer());
			score++;
		} else {
			Log.d("demoStartTrivia", "Wrong");
		}
		Log.d("demoStartTrivia", "Score=" + score);
	}

	@Override
	public void enableProgressDialog(String title, String message, int style) {
		pdQuestions = new ProgressDialog(this);
		pdQuestions.setTitle(title);
		pdQuestions.setMessage(message);
		pdQuestions.setProgressStyle(style);
		pdQuestions.show();
	}

	@Override
	public void disableProgressDialog() {
		pdQuestions.dismiss();
	}

	@Override
	public void setQuestions(List<Question> questions) {
		currentQIndex = 0;
		totalQuestions = questions.size();
		this.questions = questions;
		populateNextQuestion();
	}

	public void populateNextQuestion() {
		Question question;
		if (timer != null)
			timer.cancel();
		if (currentQIndex < totalQuestions) {
			question = questions.get(currentQIndex);
			qIndex.setText("Q" + (currentQIndex + 1));
			updateTimerText(0);
			fetchImage(question.getImageUrl(), question.getQuestionID());
			qText.setText(question.getQuestion());
			populateAnswers(question.getAnswers());
			currentQIndex++;
			timer = QuestionTimer.getInstance(this);
			timer.start();
		} else {
			Float percent = ((score * 1.0f) / totalQuestions) * 100;
			Log.d("demoStartTrivia", "Score=" + score + "\t Total Question="
					+ totalQuestions);
			Intent intent = new Intent(this, ResultActivity.class);
			intent.putExtra(PERCENT, percent.intValue());
			startActivityForResult(intent, MainActivity.RESULT_ACTIVITY);
		}
	}

	private void populateAnswers(List<String> answers) {
		answerGroup.removeAllViews();
		answerGroup.clearCheck();
		for (int i = 0; i < answers.size(); i++) {
			String ans = answers.get(i);
			RadioButton rButton = new RadioButton(this);
			rButton.setText(ans);
			rButton.setChecked(false);
			rButton.setId(i);
			answerGroup.addView(rButton);
		}
	}

	public void updateTimerText(long millisUntilFinished) {
		long timeLeft = millisUntilFinished;
		timeLeft = timeLeft / 1000;
		qTimer.setText("Time left: " + timeLeft + " seconds");
	}

	private void fetchImage(String imageUrl, int qID) {
		if (!Util.isEmpty(imageUrl)) {
			qImage.setVisibility(View.INVISIBLE);
			progressBarLayout.setVisibility(View.VISIBLE);
			new FetchImageAsyncTask(this).execute(imageUrl, qID + "");
		} else {
			progressBarLayout.setVisibility(View.GONE);
			qImage.setImageResource(R.drawable.no_image);
			qImage.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void setImage(Bitmap image, String qId) {
		Log.d("demoStartTrivia", "Received image for " + qId);
		int id = Integer.parseInt(qId);
		Question question = questions.get(currentQIndex - 1);
		if (question.getQuestionID() == id) {
			progressBarLayout.setVisibility(View.GONE);
			if (image != null)
				qImage.setImageBitmap(image);
			else
				qImage.setImageResource(R.drawable.no_image);
			qImage.setVisibility(View.VISIBLE);
		} else {
			Log.d("demoStartTrivia", "Image for Question " + qId + " was too late");
		}
	}

	@Override
	public void onBackPressed() {
		if (timer != null)
			timer.cancel();
		super.onBackPressed();
		return;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		finish();
	}

	@Override
	public void enableImageLoadProgress(String title, String message, int style) {
	}

	@Override
	public void disableImageLoadProgress() {
	}
}
