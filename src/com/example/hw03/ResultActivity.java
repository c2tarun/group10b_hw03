package com.example.hw03;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ResultActivity extends Activity {
	
	ProgressBar resultBar;
	TextView resultText;
	
	Button quitButton;
	Button tryAgainButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		resultBar = (ProgressBar) findViewById(R.id.resultBar);
		resultText = (TextView) findViewById(R.id.resultPercentage);
		
		quitButton = (Button) findViewById(R.id.quitResultButton);
		quitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				setResult(MainActivity.RESULT_ACTIVITY, intent);
				finish();
			}
		});
		tryAgainButton = (Button) findViewById(R.id.tryAgainButton);
		tryAgainButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ResultActivity.this, StartTrivia.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		
		Intent intent = getIntent();
		if(intent.getExtras() != null) {
			int percent = intent.getExtras().getInt(StartTrivia.PERCENT);
			resultBar.setProgress(percent);
			resultText.setText(percent + "%");
		}
	}
}
